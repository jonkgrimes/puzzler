extern crate amethyst;
extern crate genmesh;

use amethyst::assets::Loader;
use amethyst::core::cgmath::{Vector3, Deg};
use amethyst::core::cgmath::prelude::InnerSpace;
use amethyst::core::transform::Transform;
use amethyst::ecs::World;
use amethyst::prelude::*;
use amethyst::renderer::{AmbientColor, Camera, DisplayConfig, DrawShaded, Event, KeyboardInput,
                         Light, Mesh, Pipeline, PointLight, PosNormTex, Projection, RenderBundle,
                         RenderSystem, Rgba, Stage, VirtualKeyCode, WindowEvent};
use genmesh::{MapToVertices, Triangulate, Vertices};
use genmesh::generators::Cube;

const BACKGROUND_COLOR: [f32; 4] = [0.0, 0.0, 0.0, 0.0]; // black
const MODEL_COLOR: [f32; 4] = [0.0, 0.0, 1.0, 1.0]; // blue
const LIGHT_POSITION: [f32; 3] = [2.0, 2.0, -2.0];
const LIGHT_RADIUS: f32 = 5.0;
const LIGHT_INTENSITY: f32 = 3.0;
const AMBIENT_LIGHT_COLOUR: Rgba = Rgba(0.01, 0.01, 0.01, 1.0); // near-black
const POINT_LIGHT_COLOUR: Rgba = Rgba(1.0, 1.0, 1.0, 1.0); // white

struct Example;

impl State for Example {
    fn on_start(&mut self, world: &mut World) {
        initialize_camera(world);
        initialize_model(world);
        initialize_light(world);
    }

    fn handle_event(&mut self, _: &mut World, event: Event) -> Trans {
        match event {
            Event::WindowEvent { event, .. } => match event {
                WindowEvent::KeyboardInput {
                    input:
                        KeyboardInput {
                            virtual_keycode: Some(VirtualKeyCode::Escape),
                            ..
                        },
                    ..
                } => Trans::Quit,
                _ => Trans::None,
            },
            _ => Trans::None,
        }
    }
}

fn run() -> Result<(), amethyst::Error> {
    let path = format!(
        "{}/resources/display_config.ron",
        env!("CARGO_MANIFEST_DIR")
    );
    let config = DisplayConfig::load(&path);

    let resources = format!(
        "{}/resources/",
        env!("CARGO_MANIFEST_DIR")
    );

    let pipe = Pipeline::build().with_stage(
        Stage::with_backbuffer()
            .clear_target(BACKGROUND_COLOR, 1.0)
            .with_pass(DrawShaded::<PosNormTex>::new()),
    );

    let mut game = Application::build(resources, Example)?
        .with_bundle(RenderBundle::new())?
        .with_local(RenderSystem::build(pipe, Some(config))?)
        .build()
        .expect("Fatal error");

    game.run();

    Ok(())
}

fn main() {
    if let Err(e) = run() {
        println!("Error occurred during game execution: {}", e);
        ::std::process::exit(1);
    }
}

fn gen_cube() -> Vec<PosNormTex> {
    Cube::new()
        .vertex(|(x, y, z)| {
            PosNormTex {
                position: [x, y, z],
                normal: Vector3::from([x, y, z]).normalize().into(),
                tex_coord: [0.1, 0.1],
            }
        })
        .triangulate()
        .vertices()
        .collect()
}

fn initialize_model(world: &mut World) {
    use amethyst::assets::Handle;
    use amethyst::renderer::{Material, MaterialDefaults};

    let (mesh, material) = {
        let loader = world.read_resource::<Loader>();
        
        let mesh: Handle<Mesh> =
            loader.load_from_data(gen_cube().into(),(), &world.read_resource());

        let albedo = MODEL_COLOR.into();
        let tex_storage = world.read_resource();
        let mat_defaults = world.read_resource::<MaterialDefaults>();

        let albedo = loader.load_from_data(albedo, (), &tex_storage);

        let mat = Material {
            albedo,
            ..mat_defaults.0.clone()
        };

        (mesh, mat)
    };

    world
        .create_entity()
        .with(Transform::default())
        .with(mesh)
        .with(material)
        .build();
}

fn initialize_camera(world: &mut World) {
    use amethyst::core::cgmath::Matrix4;
    let transform = Matrix4::from_translation([-4.0, -4.0, -4.0].into()) * 
                    Matrix4::from_angle_y(Deg(225.0)) * 
                    Matrix4::from_angle_x(Deg(45.0));
    world
        .create_entity()
        .with(Camera::from(Projection::perspective(1.3, Deg(60.0))))
        .with(Transform(transform.into()))
        .build();
}

fn initialize_light(world: &mut World) {
    world.add_resource(AmbientColor(AMBIENT_LIGHT_COLOUR));
    
    let light: Light = PointLight {
        center: LIGHT_POSITION.into(),
        radius: LIGHT_RADIUS,
        intensity: LIGHT_INTENSITY,
        color: POINT_LIGHT_COLOUR,
        ..Default::default()
    }.into();

    world.create_entity().with(light).build();
}